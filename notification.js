function ajouteNotification(notification) {

    $('#nb_notification').text(parseInt($('#nb_notification').text())+1);
    args=notification;
    $('#notifications_panel ul.menu').append(Mustache.render('<li>'+window['templates']['notification_simple.html']()+'</li>', args));

}




function initialisation_notification(notifications) {

    if (notifications){
        if (notifications.length > 0) {
            $("#notifications_panel > a").after("<span class=\"badge badge-warning\" id=\"nb_notification\">0</span>");
            for (i = 0; i < notifications.length; i++) {
                ajouteNotification(notifications[i]);
            }

        }
    }
    $('#notifications_panel .ul.menu').slimScroll({
        height: '200px'
    });
}