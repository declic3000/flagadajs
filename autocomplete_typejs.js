

// avec Avec Typehead

$.fn.extend({
    'initialisation_champs_autocomplete': initialisation_champs_autocomplete,
    'initialisation_champs_autocomplete_automatique': initialisation_champs_autocomplete_automatique,
    'initialisation_champs_autocomplete_objet': initialisation_champs_autocomplete_objet,
    'initialisation_champs_autocomplete_recherche': initialisation_champs_autocomplete_recherche,
    'initialisation_champs_autocomplete_ville_cp': initialisation_champs_autocomplete_ville_cp
});




var charMap = {
    'a': /[àáâã]/gi,
    'c': /[ç]/gi,
    'e': /[èéêë]/gi,
    'i': /[ïí]/gi,
    'o': /[ôó]/gi,
    'oe': /[œ]/gi,
    'u': /[üú]/gi
};

var normalize = function (str) {
    $.each(charMap, function (normalized, regex) {
        str = str.replace(regex, normalized);
    });

    return str;
};

var queryTokenizer = function (q) {
    var normalized = normalize(q);
    return Bloodhound.tokenizers.whitespace(normalized);
};

function escaping(chaine) {
    return chaine.replace(/([ #;?&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1');
}

function initialisation_champs_autocomplete(cible, nom_variable, champs_id, url, f_affichage, msg_aucun_resultat, fident) {
    let conteneur_form = this;
    let nom_champs_cible = $(cible, conteneur_form).attr('name');
    if (champs_id !== "" && nom_champs_cible !== undefined) {
        let nom_champs_cible2 = $(champs_id, conteneur_form).attr('name');
        if (nom_champs_cible2 === undefined) {
            let last_pos = nom_champs_cible.lastIndexOf('[');
            if (last_pos > -1) {
                champs_id2 = nom_champs_cible.substr(0, last_pos) + '[' + champs_id + ']';
                let nom_champs_cible2 = $('input[name=' + escaping(champs_id2) + ']', conteneur_form).attr('id');
                if (nom_champs_cible2 !== undefined) {
                    champs_id = nom_champs_cible2;
                }
            }
        }
    }
    if (!fident)
        fident = autocomplete_identify;
    $(cible, conteneur_form).each(function () {
        Bloodhound.tokenizers.whitespace;
        $(this).attr('data-provide', 'typeahead');
        $(this).typeahead(null, {
            display: function (valeur) {
                var valeur0 = [];
                for (nvar in nom_variable) {
                    valeur0[nvar] = valeur[nom_variable[nvar]];
                }
                return valeur0.join(' - ')
            },
            hint: true,
            highlight: true,
            minLength: 0,
            limit: 100,
            ttl: 0,
            ajax: {
                cache: false
            },
            source: new Bloodhound({
                'datumTokenizer': Bloodhound.tokenizers.obj.whitespace(nom_variable),
                'queryTokenizer': Bloodhound.tokenizers.whitespace,
                'identify': fident,
                'cache': false,
                remote: {
                    url: url,
                    wildcard: '--QUERY--'
                }
            }),
            templates: {
                empty: '<div class="empty-message">' + msg_aucun_resultat + '</div>',
                suggestion: f_affichage
            }
        });

        if (champs_id) {
            $(this).bind('typeahead:select', function (ev, suggestion) {
                $('input#' + champs_id, conteneur_form).val(suggestion.id);
            });
        }
    });
}

function initialisation_champs_autocomplete_automatique() {

    let conteneur = this;
    $('.autocomplete_auto',conteneur).each(function () {
        var chemin = $(this).attr('data-url') + "&search[value]=--QUERY--";
        var f_affichage = $(this).attr('data-affichage');
        var message = $(this).attr('data-message');
        var cible = $(this).attr('data-cible');
        var composite_vars = $(this).attr('data-composite');
        if (composite_vars === undefined) {
            composite_vars = ['text'];
        } else {
            composite_vars = composite_vars.split(',');
        }
        if (f_affichage === undefined) {
            f_affichage = autocomplete_reponse_defaut;
        } else {
            f_affichage = window['autocomplete_reponse_' + f_affichage];
            if (f_affichage === undefined) {
                console.log('impossible de trouver la fonction d\'affichage');
            }
        }
        $(conteneur).initialisation_champs_autocomplete(this, composite_vars, cible, chemin, f_affichage, message);
    });

}

function initialisation_champs_autocomplete_objet(champs_id, objet, url) {

    if (url === undefined) {
        url = objet + '?action=autocomplete&search[value]=--QUERY--';
    }
    cible = '.autocomplete_' + objet;
    reponse = autocomplete_reponse_defaut;
    if (objet === 'individu')
        reponse = autocomplete_reponse_individu;
    if (objet === 'membre')
        reponse = autocomplete_reponse_membre;

    $(this).initialisation_champs_autocomplete(cible, ['nom'], champs_id, url, reponse, 'Aucun ' + objet + ' n\'a été trouvé');
}

function initialisation_champs_autocomplete_recherche() {

    let conteneur = this;
    $('#navbar-search-input', conteneur).each(function () {
        $(conteneur).initialisation_champs_autocomplete(this, ['id', 'nom'], '', chemin_root + 'recherche?action=json&search[value]=--QUERY--', autocomplete_reponse_recherche, 'Aucun élément ne correspond à la recherche');
        $(this).bind('typeahead:select', function (ev, suggestion) {
            window.location.href = chemin_root + suggestion.objet_url + '/' + suggestion.id;
        });

    });
}

function initialisation_champs_autocomplete_ville_cp(identif) {

    let conteneur = this;
    $('.autocomplete_cp', conteneur).each(function () {
        let conteneur_f = $(this).parents('form');
        selection_commune(this,conteneur_f, identif, 'code');
        $(this).bind('typeahead:select', function (ev, suggestion) {
            $('#' + identif + '_codepostal', conteneur_f).typeahead('val', suggestion.id);
            $('#' + identif + '_ville', conteneur_f).val(suggestion.text);
            let champ_suivant = $('#' + identif + '_ville').closest('div.form-group').nextAll(':visible').eq(0);
            $('input,select', champ_suivant).focus();
        });

    });

    $('.autocomplete_ville', conteneur).each(function () {
        let conteneur_f = $(this).parents('form');
        selection_commune(this, conteneur_f, identif, 'nom');
        $(this).bind('typeahead:select', function (ev, suggestion) {
            $('#' + identif + '_codepostal', conteneur_f).val(suggestion.id);
            $('#' + identif + '_ville', conteneur_f).typeahead('val', suggestion.text);
            champ_suivant = $('#' + identif + '_ville').closest('div.form-group').nextAll(':visible').eq(0);
            $('input,select', champ_suivant).focus();
        });
    });

}
function autocomplete_identify(obj) {
    return obj.id;
}
function autocomplete_reponse_defaut(data) {
    chaine = '<p>';
    chaine += '<strong>' + data.text + '</strong>';
    if (data.cplt !== undefined) {
        chaine += ' <span class="cplt">' + data.cplt + '</span>';
    }
    chaine += ' - <small>' + data.id + '</small>';
    chaine += '</p>';
    return chaine;
}

function autocomplete_reponse_recherche(data) {
    description = '';
    if (data.description) {
        description = data.description;
    }
    return '<a href="' + chemin_root + data.objet_url + '/' + data.id + '"><p>' + data.objet + ' - ' + data.id + ' <strong>' + data.nom + '</strong> ' + description + '</p></a>';
}



function selection_commune(composant, conteneur_f, identif, champs) {

    $(composant, conteneur_f).attr('data-provide', 'typeahead');

    $(conteneur_f).initialisation_champs_autocomplete(
        composant,
        champs,
        '',
        chemin_web + '/geo/codepostal?action=autocomplete&search[value]=--QUERY--',
        autocomplete_reponse_cp,
        'Ce code postal n\'existe pas dans la base',
        function (obj) {
            return obj.id;
        }
    );
}


function autocomplete_reponse_cp(data) {
    nom_suite = '';
    if (data.nom_suite)
        nom_suite = data.nom_suite;
    return '<p>' + data.id + ' <strong>' + data.text + '</strong> ' + nom_suite + '</p>';

}