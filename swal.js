function swal_alert(titre,texte,icon='error',dangerMode=true) {

    return Swal.fire({
        title: titre,
        icon: icon,
        html: texte,
        'dangerMode': dangerMode,
        showCloseButton: true
    });
}


function swal_confirm(titre,texte,icon='question',dangerMode=false) {

    return Swal.fire({
        title: titre,
        icon: icon,
        html: texte,
        dangerMode: dangerMode,
        showCloseButton: false,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText:'<i class="fa fa-thumbs-up"></i> Valider',
        confirmButtonAriaLabel: 'valider',
        cancelButtonText:'<i class="fa fa-thumbs-down"></i>',
        cancelButtonAriaLabel: 'cancel'
    });


}



