$.fn.extend({
    'initialisation_tri_colonne': initialisation_tri_colonne
    });


function initialisation_tri_colonne(){

    let conteneur = this;
    $('.choix_tri_colonne',conteneur).wrap('<div class="plage_choix_tri_colonne"></div>').after(
        '<div class="colonne_choisi"><ul></ul></div>' +
        '<div class="colonne_non_choisi"><ul></ul></div>' +
        '<div class="clearfix"></div>').hide();

    $('.plage_choix_tri_colonne',conteneur).each(function(){

        plage = $(this);
        colonnes = JSON.parse($('input.choix_tri_colonne',plage).attr('data-colonnes'));
        colonne_choisis = $('input.choix_tri_colonne',plage).val().split(";");

        for(var i = 0; i < colonne_choisis.length; i++){
            if(colonnes[colonne_choisis[i]]!==undefined){
                $('.colonne_choisi ul',plage).append('<li data-value="'+colonne_choisis[i]+'">'+colonnes[colonne_choisis[i]]+'</li>');
            }
        }

        for( i in colonnes){
            if ($.inArray(i,colonne_choisis)==-1){
                $('.colonne_non_choisi ul',plage).append('<li data-value="'+i+'">'+colonnes[i]+'</li>');
            }
        }

        new Sortable($('.colonne_choisi ul',plage)[0], {
            group: 'shared',
            animation: 150,
            onSort:choix_tri_colonne_changement_ordre
        });

        new Sortable($('.colonne_non_choisi ul',plage)[0],{
            group: 'shared', // set both lists to same group
            animation: 150,
        });
    });
}

function choix_tri_colonne_changement_ordre(evt){
    var tab=[];
    var itemEl = evt.item;
    plage = $(itemEl).parent().parent().parent();
    li_colonne = $('.colonne_choisi ul li',plage);
    for(var i = 0; i < li_colonne.length; i++){
        tab.push($(li_colonne).eq(i).attr('data-value'));
    }
    $('input.choix_tri_colonne',plage).val(tab.join(';'));
}

