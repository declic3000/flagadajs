$.fn.extend({
    'initialisation_trier_par': initialisation_trier_par,
    'initialisation_form_trier_par': initialisation_form_trier_par,
    'initialisation_trier_par_datatable': initialisation_trier_par_datatable
});

function initialisation_form_trier_par(){

    $('input.trier_par:not(.initiated)',this).each(function(){
        $(this).wrap('<div></div>');
        let div_trier_par = $(this).parent().addClass('trier_par');
        $(div_trier_par).append(Mustache.render(
            window['templates']['trier_par_dropdown.html'](), {
                'name': $(this).attr('name'),
                'selecteur':'trier_par_selection'
            }));
        let tmp = $(this).val();
        let tab_crit = {};
        if(tmp.indexOf(',')==-1){
            tab_crit0 = tmp.split(",");
        }
        else{
            tab_crit0 = tmp.split(",");
        }

        for( crit in tab_crit0){
            tmp = tab_crit0[crit].split(" ");
            tab_crit[tmp[0]]=[(parseInt(crit)+1),tmp[1]];
        }
        let options = JSON.parse($(this).attr('data-options'));


        for (key in options) {
            let classes='';
            let order='';
            let sens='';

            if(typeof tab_crit[key+''] != 'undefined'){
                classes= ' active';
                order = ' data-order="'+(tab_crit[key][0])+'" ';
                sens = ' data-sens="'+tab_crit[key][1]+'" ';
            }
            $('div.trier_par_selection', div_trier_par).append('<a class="dropdown-item'+classes+'" data-value="' + key + '" '+order+sens+'>' + options[key] + '</a>');
        }
        $(this).addClass('initiated').hide();
    });
    $(this).parent().initialisation_trier_par()
}


function initialisation_trier_par_datatable() {

    $('.trier_par button',this).each(function(){
        $(this).before('<input name="trier_par_datatable" class="trier_par d-none" type="text" value="" />');
        $(this).parents('div.trier_par').initialisation_trier_par()
    })

}




function initialisation_trier_par() {

    let selecteur =$( 'div.trier_par_selection',this)
    $('a',selecteur).click(function () {
        let selecteur = $(this).parent()
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            let ordre = $(this).attr('data-order');
            $(this).removeAttr('data-order');
            $('div.trier_par_selection a.active').each(function () {
                if ($(this).attr('data-order') > ordre){
                    $(this).attr('data-order', $(this).attr('data-order') - 1);
                }
            });
        } else {
            $(this).addClass('active');
            nb = $(' a.active',selecteur).length;
            $(this).attr('data-order', nb);
        }
        let conteneur_tri = $(this).parents('.trier_par');
        lister_trier_par(selecteur,conteneur_tri);
        trier_par_mettre_a_jour_valeur(conteneur_tri);
    });



    $('div.tri_combinaison a',this).click(function () {
        let conteneur_tri =  $(this).parents(".trier_par");
        $('div.tri_combinaison a',conteneur_tri).removeClass('active');
        $(this).addClass('active');
        $('div.trier_par_selection a',conteneur_tri).removeClass('active');
        str_champs =  $(this).attr('data-champs');
        var tab_champs = str_champs.split(";");
        for( i in tab_champs){
            t = tab_champs[i].split(" ");
            sel = 'div.trier_par_selection a[data-value='+t[0]+']';
            $(sel,conteneur_tri).addClass('active').attr('data-order',parseInt(i)+1).attr('data-sens',t[1]);
        }
        lister_trier_par(selecteur,conteneur_tri);
        trier_par_mettre_a_jour_valeur(conteneur_tri);
    });


    lister_trier_par(selecteur,this);
    $(selecteur,this).addClass('initiated');

}

// Afficher les criteres actif de tri et leur affecter des comportements
function lister_trier_par(selecteur,conteneur_f){

    $('.valeur',conteneur_f).html('');
    let nb = $(' a.active',selecteur).length;
    for(i=1;i<=nb;i++){
        $('a.active[data-order='+i+']',selecteur).each(function(){
            conteneur_tri = $(this).parents(".trier_par");
            class_ico ='fa-sort-down';
            data_sens='DESC';
            if($(this).attr('data-sens')==='ASC'){
                class_ico ='fa-sort-up';
                data_sens='ASC';
            }
            span = $('<span class="tri_clause" data-value="'+$(this).attr('data-value')+'" data-sens="'+data_sens+'"><span class="nom">'+$(this).html()+' <i class="fa '+class_ico+'"></i></span></span>');
            $('.valeur',conteneur_tri).append(span);

            $(span).prepend('<span class="delete"><i class="fa fa-times"></i></span>');

            $('.nom',span).click(function(){
                if ($(this).parent().attr('data-sens')==='DESC') {
                    $('svg.fa-sort-down',this).remove();
                    $(this).append('<i class="fa fa-sort-up"></i>');
                    $(this).parent().attr('data-sens','ASC');
                }else{
                    $('svg.fa-sort-up',this).remove();
                    $(this).append('<i class="fa fa-sort-down"></i>');
                    $(this).parent().attr('data-sens','DESC');
                }
                conteneur_tri = $(this).parents(".trier_par");
                trier_par_mettre_a_jour_valeur(conteneur_tri);

            });
            $(span).hover(function(){
                    $('.delete',this).show();
                },
                function(){
                    $('.delete',this).hide();
                }
            );
            $('.delete',span).hide().click(function(){

                valeur = $(this).parent().attr('data-value');
                $('a.active[data-value='+valeur.replace('.','\\.')+']',selecteur).removeClass('active');
                let nb=$('a.active',selecteur).length;
                if(nb===0){
                    $('.etat',conteneur_f).hide();
                }
                conteneur_tri = $(this).parents(".trier_par");
                $(this).parent().remove();
                trier_par_mettre_a_jour_valeur(conteneur_tri);

            });
        });
    }
    $('.etat',conteneur_f).show();
    $('.etat',conteneur_f).fadeIn();
}

function trier_par_mettre_a_jour_valeur(conteneur_tri){

    let champs_texte = $('input[type=text][name=trier_par_datatable]',conteneur_tri);

    if (champs_texte.length > 0){

        let result = trier_par_formate_resultat(conteneur_tri);
        $(champs_texte).attr('value',result);
        $.event.trigger({type: "rafraichirDatatable" });
    }
    else{
        result=[];
        $('span.tri_clause',conteneur_tri).each(function(){
            result.push($(this).attr('data-value')+' '+$(this).attr('data-sens'));
        });
        $('input[type=text]',conteneur_tri).attr('value',result.join(','));
    }
}


function trier_par_formate_resultat(conteneur_tri){

    result=[];
    $('span.tri_clause',conteneur_tri).each(function(){
        result.push($(this).attr('data-value')+'-sens-'+$(this).attr('data-sens'));
    });
    return result.join(',');
}

