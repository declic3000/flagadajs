$.fn.extend({
    'initialiseXeditable': initialiseXeditable
});


function initialiseXeditable() {

    $('.xeditable', this).each(function () {
        $(this).editable({
            ajaxOptions: {
                dataType: 'json'
            },
            send: 'always',
            success: function (response, newValue) {
                if (!response) {
                    return "Unknown error!";
                }

                if (response.success === false) {
                    return response.msg;
                }
            }
        })
    });
}
