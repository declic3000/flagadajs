$.fn.extend({
    'initialisation_bouton_lievre_tortue': initialisation_bouton_lievre_tortue
});

function initialisation_bouton_lievre_tortue() {
    let conteneur_f = this;
    let conteneur_form_div = $(this).parent('div.formulaire');
    let checked = $(conteneur_form_div).hasClass('lievre');
    let html = '';
    if (checked) {
        html = 'checked="checked"';
    }
    $('.secondaire', this)
        .closest('div.formulaire')
        .children('div.en_haut_a_droite')
        .prepend(
            '<div class="tortue_lievre pretty p-icon p-toggle p-plain">\n' +
            '	<input id="switch_tortue_lievre" type="checkbox" ' + html + ' />\n' +
            '	<div class="state text-success p-off">\n' +
            '		<i class="fa fa-tortue"></i>' +
            '	</div>' +
            '	<div class="state text-warning p-on">\n' +
            '		<i class="fa fa-lievre"></i>\n' +
            '	</div>\n' +
            '</div>');

    if (checked) {
        $('.secondaire', conteneur_f).closest('.form-group').slideUp();
    }

    $('.tortue_lievre input[type=checkbox]').change(function () {
        if ($(this).prop("checked")) {
            $('.secondaire', conteneur_f).closest('.form-group').slideUp();
        } else {
            $('.secondaire', conteneur_f).closest('.form-group').slideDown();
        }
    });
}

