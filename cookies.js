$.fn.extend({
    'initialisation_cookies_tabs': initialisation_cookies_tabs
});

function initialisation_cookies_tabs(){

    let conteneur = this;
    $('.tabs_cookie',conteneur).each(function(){
        id  = $(this).attr('id')
        num = Cookies.get('tabs_'+id);
        if(num>=0){
            $('.nav-tabs li:nth-child('+num+') a',this).tab('show');
        }

        $('.nav-tabs li',this).click(function(){
            parent = $(this).parent();
            id  = $(parent).parent().attr('id');
            num = $( "li" ,parent).index( this )+1;
            Cookies.set('tabs_'+id, num);
        });

    })

}


