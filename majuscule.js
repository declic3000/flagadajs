$.fn.extend({
    'activer_majuscule_magique': activer_majuscule_magique
});

function activer_majuscule_magique(){


    $('input.majuscule_magique', this).each(function() {
        valeur = parseInt($(this).attr('data-majuscule'));
        $(this).wrap('<div class="input-group"></div>');
        $(this).after('<span class="input-group-btn"><button type="button" tabIndex="-1" class="btn-majuscule_magique btn btn-default" value="'+valeur+'"></button></span>');
        bouton = $(this).next('span').children('button.btn-majuscule_magique');
        majuscule_magique_affichage(this,bouton,valeur);

    });
    $('button.btn-majuscule_magique', this).click(function() {
        valeur= parseInt($(this).val())+1;
        if (valeur>2){valeur=0;}
        champs = $(this).parent().prev('input');
        majuscule_magique_affichage(champs,$(this),valeur);
        pref= $(champs).attr('data-pref');
        $(this).val(valeur);
        $.getJSON(url_preference_edit.replace('++++',pref), {'valeur': valeur});
        return false;

    });


}

function majuscule_magique_affichage(champs,bouton,valeur){


    if (valeur>0){
        $(bouton).addClass('btn-primary').removeClass('btn-default');
    }
    else {
        $(bouton).removeClass('btn-primary').addClass('btn-default');
    }
    if (valeur<2) {
        $('svg', bouton).remove();
        $(bouton).append('<i class="fa fa-uppercase"></i>');
    } else {
        $('svg', bouton).remove();
        $(bouton).append('<i class="fa fa-lowercase"></i>');
    }
    if (valeur===0){
        $(champs).css("text-transform",'none');
    }
    else if (valeur===1){
        $(champs).css("text-transform",'uppercase');
    }
    else{
        $(champs).css("text-transform",'capitalize');
    }

}



function ucFirstAllWords( str )
{
    var pieces = str.split(" ");
    for ( var i = 0; i < pieces.length; i++ )
    {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1).toLowerCase();
    }
    return pieces.join(" ");
}


function champs_majuscule_css(champs){

    valeur = $(champs).val();
    css = $(champs).css('text-transform');
    if ( css ==='uppercase' ) {
        valeur = valeur.toUpperCase();
    } else if ( css ==='capitalize' ) {
        valeur = ucFirstAllWords(valeur);
    }
    return valeur;

}
