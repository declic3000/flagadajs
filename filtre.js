$.fn.extend({
    'initialisation_filtre': initialisation_filtre
});

function initialisation_filtre(){
    $('.filtres:not(.initiated)').each(function(){
        $(this).addClass('initiated');
        $('.filtre',this).on('click',function(){
            $(this).toggleClass('active');
            filtre_rafraichir($(this).parents('.filtres'));
        });
        filtre_rafraichir(this);
    })
}


function filtre_rafraichir(container){

    let cible = $(container).data('cible');
    cible = $(cible);
    if ($('.filtre.active',container).length>0) {
        $('.filtre_ob', cible).hide();
        let valeur_filtre = {}
        $('.filtre.active',container).each(function() {
            let groupe = $(this).data('filtre-groupe');
            if (valeur_filtre[groupe] === undefined){
                valeur_filtre[groupe]=[];
            }
            valeur_filtre[groupe].push( $(this).data('filtre-valeur'));
        });
        $('.filtre_ob',cible).each(function(){

            let afficher = true
            for(groupe in valeur_filtre) {
                valeurs = valeur_filtre[groupe];
                let tab_valeur = $(this).data('filtre-valeur-'+groupe).split('|');
                afficherv = false;
                for (v in valeurs) {
                    let valeur = valeurs[v];
                    if ($.inArray(valeur, tab_valeur) !== -1) {
                        afficherv = true
                    }
                }
                afficher = afficher && afficherv;
            }
            if (afficher){
                $(this).show();
            }
        });
    }
    else{
        $('.filtre_ob', cible).show();
    }
    $('.cat_filtre',cible).each(function(){
        let div = $(this).next('div.evt_date');
        if ($('div.filtre_ob:visible',div).length===0){
            $(this).hide();
        }else{
            $(this).show();
        }
    })
}