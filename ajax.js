$.fn.extend({
    'initialisation_ajax': initialisation_ajax,
    'initialisation_ajax_declencheur': initialisation_ajax_declencheur,
    'initialisation_ajax_remplace': initialisation_ajax_remplace,
    'initialisation_ajax_redirect': initialisation_ajax_redirect,
    'initialisation_ajax_btn_switch': initialisation_ajax_btn_switch,
    'initialisation_ajax_form': initialisation_ajax_form,
    'initialisation_lazyload': initialisation_lazyload
});


function initialisation_ajax() {
    $(this).initialisation_ajax_declencheur();
    $(this).initialisation_ajax_remplace();
    $(this).initialisation_ajax_redirect();
    $(this).initialisation_ajax_btn_switch();
    $(this).initialisation_ajax_form();
    $(this).initialisation_lazyload();
}


function initialisation_lazyload() {

    let conteneur = this;
    $("div.lazyload[data-url]:not(.initiated)", conteneur).each(function () {
        url = $(this).attr('data-url');
        $.ajax({
            dataType: "html",
            url: url,
            context: $(this),
            success: function (html) {
                $(this).html(html);
                $(this).initialisation_js();
            }
        });
        return false;
    });

}


function initialisation_ajax_declencheur() {

    let conteneur = this;
    $("a.ajax-declencheur[data-cible]:not(.initiated)", conteneur).click(function () {
        url = $(this).attr('href');
        cible = $(this).attr('data-cible');
        $.ajax({
            dataType: "json",
            url: url,
            context: $(cible),
            success: function (data) {
                if (data.ok) {
                    if (data.message !== undefined) {
                        if (typeof toastr !== 'undefined' && typeof data.message !== 'undefined') {
                            toastr.success(data.message, 'SUCCESS');
                        }
                    }
                    if (data.declencheur_js !== undefined) {
                        window[data.declencheur_js](data);
                    }
                }
            }
        });
        return false;
    }).addClass('initiated');
}


function initialisation_ajax_remplace() {

    let conteneur = this;
    $("a.ajax-remplace[data-cible]:not(.initiated)", conteneur).click(function () {
        url = $(this).attr('href');
        cible = $(this).attr('data-cible');
        $.ajax({
            dataType: "json",
            url: url,
            context: $(cible),
            success: function (data) {
                $(cible).replaceWith(data.html);
                $(cible).initialisation_js();
            }
        });
        return false;
    }).addClass('initiated');

    $("select.ajax-remplace[data-cible]:not(.initiated)", conteneur).change(function () {
        url = $(this).attr('data-href');
        valeur = $(this).val();


        $.ajax({
            url: url + valeur,
            context: this,
            success: function (data) {
                cible = $(this).attr('data-cible');
                $(cible).replaceWith(data);
                $(cible).parent().initialisation_ajax_remplace();
            }
        });
        return false;
    }).addClass('initiated');
}


function initialisation_ajax_redirect() {
    let conteneur = this;
    $("a.ajaxjson-redirect:not(.initiated)", conteneur).click(function () {
        url = $(this).attr('href');
        $.getJSON(url).done(function (data) {
            if (data.ok) {
                if (typeof toastr !== 'undefined' && typeof data.message !== 'undefined') {
                    toastr.success(data.message, 'SUCCESS');
                }
                if (data.redirect) {
                    setTimeout(function () {
                        redirection(data.redirect)
                    }, 1000);
                }
            }
        }).fail(function (jqxhr, textStatus, error) {
            let reponse = JSON.parse(jqxhr.responseText);
            swal_alert('Erreur', reponse.message);
        });
        return false;
    }).addClass('initiated');
}

function initialisation_ajax_btn_switch() {
    let conteneur = this;
    $('.btn-ajax-switch:not(.initiated)', conteneur).each(function () {
        $(this).attr('data-href', $(this).attr('href'));
        $(this).removeAttr('href');
        $(this).on('click',function () {
            $.ajax({
                url: $(this).attr('data-href'),
                dataType: 'json',
                context: this,
                success: function (r) {
                    if (r['ok']) {
                        if ($(this).hasClass('btn-primary')) {
                            $(this).removeClass('btn-primary');
                            $(this).addClass('btn-secondary');
                        } else {
                            $(this).removeClass('btn-secondary');
                            $(this).addClass('btn-primary');
                        }
                        $(this).attr('data-href', r['url']);
                    }
                }
            });
        })
    }).addClass('initiated');
}

function initialisation_ajax_form() {

    let conteneur = this;
    $("form.ajaxjson_form:not(.initiated)", conteneur).each(function () {
        let options = {
            dataType: 'json',
            success: modal_reaction_ajaxform,
            context: this,
            clearForm: false
        };
        $(this).ajaxForm(options);
        $(this).addClass('initiated');
    });
}

function reponse_ajax(data, status,place,place_container){

    if (status === 'success') {
        let delete_callback = $(place_container).data('js-callback-remove');
        if (delete_callback === undefined) {
            $(place).html('');
        }else{
            $('button.cliclic[type=submit]',place_container).removeClass('en_attente').removeClass('cliclic');
        }

        if (data.ok) {
            if (data.message !== undefined) {
                $(place).prepend('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i> ' + data.message + '</h4></div>')
            }
            if (data.modif_html !== undefined) {
                for (let nom in data.modif_html) {
                    $('#' + nom).html(data.modif_html[nom]);
                }
            }
            if (delete_callback !== undefined) {
                $(delete_callback).remove();
            }
            if (data.declencheur_js !== undefined) {
                window[data.declencheur_js](data);
            }
            if (data.redirect !== undefined) {
                setTimeout("redirection('" + data.redirect + "')", 800);
            }

        } else {
            if (data.message!== undefined) {
                $(place).html('<div class="alert alert-danger alert-dismissible"><h4><i class="icon fa fa-blink fa-exclamation-triangle"></i> ' + data.message + '</h4></div>')
            }
        }
        if (data.html !== undefined) {
            $(place,place_container).html(data.html);
            if (typeof init_js === "function") {
                init_js(place_container);
            }
            $('form', place).on('submit',function(event){
                if ($(this).hasClass('has_error')) {
                    $("button[type=submit]",this).removeClass('en_attente');
                    return false;
                } else {
                    // recupérer la valeur des champs
                    let data  = $(this).formToArray();
                    // Ajouter la valeur du bouton
                    let bt_submit = $("button.cliclic[type=submit]",this);
                    if (bt_submit){
                        data.push({
                            'name': bt_submit.attr('name'),
                            'type': 'submit',
                            'value': bt_submit.val(),
                            'required':false
                        });
                    }
                    let options = {
                        url: $(this).attr("action"),
                        data: data,
                        method: $(this).attr('method'),
                        dataType: 'json',
                        context: place_container
                    }
                    $.ajax(options)
                        .done(modal_reaction)
                        .fail(modal_reaction_erreur);
                }
                return false;
            });
        }

    }
}
