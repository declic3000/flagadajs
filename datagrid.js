function affichage_grille(msg, opt) {

	newitem = '';
	colonne = opt['colonnes'];
	if (msg.recordsTotal === 0) {
		$('#no_results').removeClass('hide');
		$('#stat').hide();
	} else {
		$('#no_results').addClass('hide');
		$('#stat').show();
		$('#stat strong').html(msg.recordsTotal);

		for (i = 0; i < msg.data.length; i++) {
			var args2 = {};

			for (j = 0; j < colonne.length; j++) {
				nom = colonne[j].name;
				nom = nom.replace(".", "_");
				args2[nom] = msg.data[i][j];
			}


			redirect = '';
			if (opt['redirection'])
				redirect = '?redirect=' + opt['redirection'];

			cloture = false;
			var id = args2[opt['cle']];
			args2 = $.extend(args2, opt);

			for (var bt_nom in opt['bt']) {
				args2['bt_'+bt_nom+'_url'] = opt['bt'][bt_nom]['url'].replace('--id--', id)
			}
			if (args2['motsIndividu'] !== undefined) {

				for (k = 0; k < args2['motsIndividu'].length; k++) {
					args2['motsIndividu'][k] = {
						url : '',
						'nom' : getMot(args2['motsIndividu'][k])['nom']
					};
				}
			}

			if (args2['motsMembre'] !== undefined) {
				for (k = 0; k < args2['motsMembre'].length; k++) {
					args2['motsMembre'][k] = {
						url : '',
						'nom' : getMot(args2['motsMembre'])['nom']
					};
				}
			}

			var template = 'membrind.html';
			if (opt['template'] !==undefined) {
				template = opt['template'];
			}
			newitem += Mustache.render(window['templates'][template](),args2);
		}

		var $items = $(newitem);
		$items.imagesLoaded(function() {
			if ((number_request-1)==msg.draw){
				$grid.infiniteScroll('appendItems', $items).masonry('appended',	$items).masonry();

				$items.initialisation_modal();
				if (typeof datagrid_cplt === "function") {
					datagrid_cplt($items);
				}
			}
		});


	}
}
