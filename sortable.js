$.fn.extend({
    'initialisation_sortable': initialisation_sortable
});


let sortable_mouvement=[];
function initialisation_sortable() {

    $(".sortable_tlt", this).each(function () {
        Sortable.create(this, {
            'handle': '.drag-handle',
            'onUpdate': function (evt) {
                href = $(evt.target).attr('data-href');
                if (href.indexOf("?") === -1) {
                    href += '?';
                } else {
                    href += '&';
                }
                href += 'bloc_id=' + $(evt.item).attr('data-id') + '&index=' + $(evt.item).index();
                $.getJSON(href, function (data) {
                    sortable_reaction(data);

                })

            }
        });
    });



    $(".sortable_mvt", this).each(function () {
        let cible_btn = $(this).attr('data-cible-btn')
        Sortable.create(this, {
            'handle': '.drag-handle',
            'onUpdate': function (evt) {
                if (sortable_mouvement.length===0){
                    $(cible_btn).append('<button id="btn-sortable-mvt" class="btn btn-primary">Enregistrer le nouvel ordre</button>');
                    $(cible_btn +' #btn-sortable-mvt').click(function(){
                        href = $(evt.target).attr('data-href');
                        if (href.indexOf("?") === -1) {
                            href += '?';
                        } else {
                            href += '&';
                        }
                        href += 'sortable_mouvement='+sortable_mouvement.join('|');
                        $.getJSON(href, function (data) {
                            sortable_reaction(data);
                            if(data.ok){
                                sortable_mouvement=[];
                                $(cible_btn +'#btn-sortable-mvt').remove();
                            }
                        })
                    });

                }
                sortable_mouvement.push($(evt.item).attr('data-id')+'-'+$(evt.item).index())
            }
        });
    });



}


function sortable_reaction(data){


    if (data.ok) {
        if (data.declencheur_js !== undefined) {
            window[data.declencheur_js](data);
        }
        if (data.redirect !== undefined) {
            setTimeout("redirection('" + data.redirect + "')", 800);
        }

    }

}


