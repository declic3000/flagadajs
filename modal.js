$.fn.extend({
    'initialisation_modal': initialisation_modal,
    'initialisation_modal_btn': initialisation_modal_btn,
    'initialisation_modal_form': initialisation_modal_form,
    'initialisation_modal_confirm': initialisation_modal_confirm,
    'initialisation_modal_iframe': initialisation_modal_iframe,
    'initialisation_modal_confirm_supprimer': initialisation_modal_confirm_supprimer
});



function initialisation_modal() {
    $(this).initialisation_modal_btn();
    $(this).initialisation_modal_form();
    $(this).initialisation_modal_confirm();
    $(this).initialisation_modal_iframe();
    $(this).initialisation_modal_confirm_supprimer();
}


function creer_modal(obj, titre = undefined, body = '<p></p>', footer = '',autodestruction=true) {

    if (titre === undefined) {
        if ($(obj).attr('title')!==undefined){
            titre = $(obj).attr('title');
        }
        else if ($(obj).data('title')!==undefined){
            titre = $(obj).data('title');
        }
        else{
            titre = $(obj).text();
        }
    }

    let id = $(obj).attr('id');
    let class_modal = '';
    if ($(obj).hasClass('modal-large')) {
        class_modal = 'modal-lg';
    }
    let modal = $('#modal_' + id);
    if (!$(modal).length) {
        $('body').append(genererModal('modal_'+id, titre, body, footer, true, class_modal));
        modal = $('#modal_' + id, 'body');
    }
    else{
        $('.modal-title', modal).html(titre);
    }
    $(modal).modal({show: true});
    if(autodestruction){
        $(modal).on('hidden.bs.modal', function () {
            $(this).remove();
        });
    }
    return modal;

}


function initialisation_modal_btn( selecteur = ".btn-modal", callback = undefined) {

    let conteneur = this;
    // <div id="modal" class="btn-modal" data-modal="coucou">La boite à ...</div>
    let selecteur_tmp = selecteur + '[data-modal]';
    $(selecteur_tmp, conteneur).each(function() {
        if (!$(this).is('[data-init-modal_btn]')) {
            $(selecteur + '[data-modal]', conteneur).click(function () {
                let modal = creer_modal(this);
                let content = $(this).attr('data-modal');
                $('.modal-body', modal).html(content + '<div class="clearfix"></div>');
                return false;
            });
            $(this).attr('data-init-modal_btn','ok');
        }
    });


    // <div id="modal" class="btn-modal" data-selecteur="coucou">La boite à ...</div>
    selecteur_tmp = selecteur + '[data-selecteur]';
    $(selecteur_tmp, conteneur).each(function() {
        if (!$(this).is('[data-init-modal_btn]')) {
            $(selecteur_tmp, conteneur).click(function (ev) {
                let modal = creer_modal(this,undefined,undefined,undefined,false);
                if ($(this).hasClass('initialised')===false){
                    let selector = '#' + $(this).attr('data-selecteur');
                    $('.modal-body', modal).html( $(selector));
                    $('.modal-body', modal).append('<div class="clearfix"></div>');
                    $(selector, modal).removeClass('d-none');
                    $(this).addClass('initialised');
                }
                $(modal).modal('show');
                return false;
            });
            $(this).attr('data-init-modal_btn','ok');
        }
    });


    // <a href="url" class="btn-modal">En savoir plus</div>
    selecteur_tmp = selecteur + '[href]';
    $(selecteur_tmp, conteneur).each(function() {
        if (!$(this).is('[data-init-modal_btn]')) {
            $(selecteur_tmp, conteneur).click(function () {
                let modal = creer_modal(this);
                let href = $(this).attr('href');
                $('.modal-body', modal).load(href);
                $(modal).modal('show');
                return false;
            });
            $(this).attr('data-init-modal_btn','ok');
        }
    });



    // <div data-href="url" class="btn-modal">En savoir plus</div>
    selecteur_tmp = selecteur + '[data-href]';
    $(selecteur_tmp, conteneur).each(function() {
        if (!$(this).is('[data-init-modal_btn]')) {
            $(selecteur_tmp, conteneur).click(function () {
                let modal = creer_modal(this);
                let href = $(this).attr('data-href');
                $('.modal-body', modal).load(href);
                $(modal).modal('show');
                return false;
            });
            $(this).attr('data-init-modal_btn','ok');
        }
    });



    // <div href="url" class="btn-modal-json">En savoir plus</div>
    selecteur_tmp = selecteur + '-json[href]';
    $(selecteur_tmp, conteneur).each(function() {
        if (!$(this).is('[data-init-modal_btn]')) {
            $(selecteur_tmp, conteneur).click(function () {
                let modal = creer_modal(this);
                $(modal).modal('show');
                let href = $(this).attr('href');
                let options = {
                    url: href,
                    dataType: 'json',
                    context: modal
                };
                $.ajax(options)
                    .done(modal_reaction)
                    .fail(modal_reaction_erreur);
                return false;
            });
            $(this).attr('data-init-modal_btn','ok');
        }
    });
}


function initialisation_modal_iframe( selecteur = "a.modal-iframe", callback = undefined) {

    let conteneur = this;
    $(selecteur, conteneur).click(function (ev) {
        let modal = creer_modal(this);
        let href = $(this).attr('href');
        $('.modal-body', modal).html('<iframe  src="' + href + '" height="500px" width="100%"></iframe>');
        $('.modal-footer', modal).html('<a href="' + href + '" target="_blank">Ouvrir dans un autre onglet</a>');
        return false;
    });

}


function initialisation_modal_confirm( selecteur = "a[data-confirm]", callback = undefined) {

    let conteneur = this;
    $(selecteur, conteneur).click(function (ev) {
        if($(this).is('[href]') && !($(this).is('[data-href]'))){
            $(this).attr('data-href',$(this).attr('href'));
        }
        $(this).removeAttr('href');
        let href = $(this).attr('data-href');
        let body = $(this).attr('data-confirm');
        let footer = '<button type="button" class="btn btn-default" data-dismiss="modal">non</button>' +
            '<a  class="btn btn-danger" id="dataConfirmOK" href="' + href + '">oui</a>';
        let modal = creer_modal(this, 'Confirmation', body, footer);
        let delete_callback = $(this).attr('data-js-callback-remove');
        if (delete_callback !== undefined) {
            $(modal).attr('data-js-callback-remove', delete_callback);
        }
        $(modal).modal('show');
        $('#dataConfirmOK', modal).click(function () {
            let modal_confirm = $(this).parents('#dataConfirmModal');
            options = {
                url: href,
                dataType: 'json',
                context: modal_confirm
            }
            $.ajax(options)
                .done(modal_reaction)
                .fail(modal_reaction_erreur);
            return false;
        });
        return false;
    });
}


function initialisation_modal_confirm_supprimer( selecteur = "a[data-confirm-supprimer]", callback = undefined) {

    let conteneur = this;
    $(selecteur, conteneur).each(function() {
            if (!$(this).hasClass('initiated')) {
                $(this).on('click', function (ev) {
                    let href = $(this).attr('data-href');
                    let footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>' +
                        '<a class="btn btn-danger" id="dataConfirmOK" href="#">Supprimer</a>';
                    let body = $(this).attr('data-confirm-supprimer');
                    let modal = creer_modal(this, 'Confirmation', body, footer);
                    $(modal).modal('show');
                    $('a#dataConfirmOK', modal).attr('data-token', $(this).attr('data-token'));
                    let delete_callback = $(this).attr('data-js-callback-remove');
                    if (delete_callback !== undefined) {
                        $(modal).attr('data-js-callback-remove', delete_callback);
                    }
                    $('#dataConfirmOK', modal).click(function () {
                        let modal_confirm = $(this).parents('#dataConfirmModalsupprimer');
                        $('.modal-footer', modal_confirm).hide();
                        let token = $(this).attr('data-token');
                        options = {
                            url: href,
                            data: {'_token': token},
                            dataType: 'json',
                            method: 'DELETE',
                            context: modal
                        }
                        $.ajax(options)
                            .done(modal_reaction)
                            .fail(modal_reaction_erreur);
                    });

                    return false;
                });
                $(this).addClass('initiated')
            }
        }
        );

}


function initialisation_modal_form( selecteur = "a.modal-form") {

    let conteneur = this;
    $(selecteur + ':not(.modalised)', conteneur).click(function () {

        let modal = creer_modal(this);
        $(this).addClass('modalised');
        let href = $(this).attr('href');

        $.ajax({
            dataType: "json",
            url: href,
            context: modal})
            .done(modal_reaction)
            .fail(modal_reaction_erreur);
        $(modal).attr("disabled", "disabled");
        return false;
    });

}


function genererModal(id, titre, body, footer, close, class_cplt) {

    if (class_cplt === undefined) {
        class_cplt = '';
    }

    let args = {
        'id': id,
        'titre': titre,
        'body': body,
        'footer': footer,
        'close': close,
        'class_cplt': class_cplt,
    }
    if (titre !== undefined) {
        args["titre"] = titre;
    }
    if (footer !== undefined && footer !== '') {
        args["footer"] = footer;
    }
    return Mustache.render(window['templates']['modal_bs'+flagadajs_version_boostrap+'.html'](), args);
}


function modal_reaction_ajaxform(data, status, xhr) {
    reponse_ajax(data, status, this,this);
}

function modal_reaction_erreur( jqxhr, textStatus, error ) {
    let reponse = JSON.parse(jqxhr.responseText);
    swal_alert('Erreur',reponse.message);
    $(this).modal('hide');
}


function modal_reaction(data, status) {

    let modal_form = $(this);
    let place = modal_form;
    if ($(modal_form).hasClass('modal')) {
        place = $('.modal-body', modal_form).get(0);
    }
    reponse_ajax(data, status,place,modal_form);
    if (status === 'success') {
        if (data.ok) {
            let timeout = 10;
            if (data.message !== undefined) {
                timeout = 800;
            }
            if (data.ne_pas_fermer === undefined) {
                setTimeout(function () {
                    $(this).modal('hide');
                }.bind(this), timeout);
            }
        }
        else{
        $(modal_form).modal("show");
        }
    }
}

