$.fn.extend({
    'initialisation_tabs_datatable': initialisation_tabs_datatable
});



function initialisation_tabs_datatable(){
    $('a[data-toggle="tab"]',this).on('shown.bs.tab',recalculateDataTableResponsiveSize);
}

function recalculateDataTableResponsiveSize() {
    $($.fn.dataTable.tables(true)).DataTable().responsive.recalc();
}

