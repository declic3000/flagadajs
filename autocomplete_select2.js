
/// Avec Select2
$.fn.extend({
    'initialisation_champs_autocomplete_select2': initialisation_champs_autocomplete_select2
});

function initialisation_champs_autocomplete_select2() {

    let conteneur = $(this);
    $('.autocomplete_select2',this).each(function () {
        let chemin = $(this).attr('data-url');
        let cible = $(this).attr('data-cible');
        let f_affichage = $(this).attr('data-affichage');
        if (f_affichage === undefined) {
            f_affichage = "defaut"
        }
        f_affichage = window['autocomplete_reponse_' + f_affichage];
        let message = $(this).attr('data-message');
        let tags = ($(this).attr('data-tags') !== undefined);
        champs_autocomplete_select2(this, chemin, f_affichage, message, tags);
    });
}


function champs_autocomplete_select2(elt, url, f_affichage, msg_aucun_resultat, tags) {

        let options={
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 1,
            minimumResultsForSearch: 15,
            templateResult: f_affichage
        }
        if (url) {
            options['ajax'] =
                {
                    url: url,
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            'search[value]': params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: data,
                            pagination: {
                                more: (params.page * 30) < data.length
                            }
                        };
                    },
                    cache: false
                }
        }
        if (tags){
            options['tags']= true;
        }

        let dropdownParent = $(elt).parents('.modal[role=dialog]');
        if (dropdownParent.length > 0){
           options['dropdownParent']= dropdownParent;
        }

        $(elt).select2(options);

        let cible = $(elt).data('cible');
    if (cible){
        form_parent = $(elt).parents('form');
        $(elt).on('change', function (e) {
            $(cible,form_parent).val(JSON.stringify($(this).select2('data')));
        });
        $('button[type=submit]',form_parent).on('click', function (e) {
            $('option[data-select2-tag=true]',elt).remove();
        });
    }
}
