function is_function(nom_fonction){
    return (typeof nom_fonction === "function");
}

function ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function redirection(url){
    window.location.href = url;
}

function date_formate(date_input){
    return moment(date_input, "DD/MM/YYYY").format("YYYY-MM-DD");
}

function date_deformate(date_input){
    return moment(date_input, "YYYY-MM-DD").format("DD/MM/YYYY");
}




function getParam(url,variable){

    var uri = URI(url);
    if (uri.hasQuery(variable) === true){
        tab = uri.search(true);
        return tab['variable'];
    }
    return ''
}



function financial(x) {
    return Number.parseFloat(x).toFixed(2);
}

function change_history(cle,valeur){
    var currentState = history.state;
    currentState[cle]=valeur;
    history.replaceState(currentState,"");
}
