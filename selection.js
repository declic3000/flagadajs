$.fn.extend({
    'initialisation_selection': initialisation_selection
});

function initialisation_selection() {
    $('#btn_selection',this).click(function () {
        $('#choix_selection').removeClass('d-none');
    });
    activerLigneSelection();
}

function activerLigneSelection() {

    $('#choix_selection div.selection[data-value]').click(function () {
        id = $('#choix_selection').attr('data-rel');
        values = JSON.parse($(this).attr('data-value'));
        filtres = $('#filtres .panneau');
        $('#filtre_recherche').val('');
        $('.filtres select').each(function () {
            if ($(this).val() !== '') {
                $(this).val('').trigger('change.select2')
            }
        });
        $('ul.list-expanded li.on[data-key][data-value]', filtres).removeClass('on');
        for (indice in values) {
            if (indice === 'search') {
                $('#filtre_recherche').val(values[indice]['value']);
            } else {

                selecteur_filtre = $('#filtres select[name=' + indice + ']');

                if (selecteur_filtre.length === 0)
                    selecteur_filtre = $('#filtres select[name=' + indice + '\\[\\]]');

                if (selecteur_filtre.length > 0) {
                    if (typeof values[indice] === 'string') {
                        $(selecteur_filtre).val(values[indice]).trigger('change');
                    } else {
                        if ($(selecteur_filtre).hasClass('autocomplete_select2')) {
                            ind = Array();
                            for (i in values[indice]) {
                                val = values[indice][i];
                                var chemin = $(selecteur_filtre).attr('data-url');
                                var libelle = '';
                                $.ajax({
                                    dataType: 'json',
                                    async: false,
                                    url: chemin_root + chemin + '&search[value]=' + val,
                                    success: function (result) {
                                        libelle = result[0]['text'];
                                        var newOption = new Option(libelle, val, true, true);
                                        $(selecteur_filtre).append(newOption).trigger('change');
                                        $(selecteur_filtre).trigger({
                                            type: 'select2:select',
                                            params: {
                                                data: ind
                                            }
                                        });
                                    }
                                });
                            }
                        } else {
                            $(selecteur_filtre).val(values[indice]).trigger('change');
                        }
                    }
                } else {
                    tab_value = [ values[indice] ] ;
                    if (values[indice].indexOf(",")>-1){
                        tab_value = values[indice].split(',');
                    }
                    for(i in tab_value){
                        $('li[data-key=' + indice + '][data-value=' + tab_value[i] + ']', filtres).addClass('on');
                    }
                }
            }
        }
        $('#modal_btn_selection').modal('hide');
        table[id].ajax.reload();
    });

}


function rafraichirSelection() {
    activerLigneSelection();
    $(this).initialisation_modal_confirm_supprimer('#choix_selection');
}