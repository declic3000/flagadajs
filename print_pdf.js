$.fn.extend({
	'initialisation_print_pdf': initialisation_print_pdf
});


function initialisation_print_pdf() {
	$('a.btn-print-direct[href]',this).click(function(e){
		print_pdf($(this).attr('href'));
		e.preventDefault();
		return false;
	});
}

function print_pdf(pdf) {
	let iframe = $('#impression_directe_pdf');
	if (iframe.length>0) {
		$(iframe).remove();
	}
	$("body").append('<iframe src="'+pdf+'" class="d-none" id="impression_directe_pdf"></iframe>');
	iframe= $('#impression_directe_pdf')[0];
	iframe.onload= function () {
		var iframewindow= iframe.contentWindow? iframe.contentWindow : iframe.contentDocument.defaultView;
		iframewindow.focus();
		iframewindow.print();
	};
	return false;
}

function print_pdf_form(data) {
	return print_pdf(data['pdf']);
}