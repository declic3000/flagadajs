$.fn.extend({
    'initialisation_clipboard': initialisation_clipboard
});

function initialisation_clipboard(){

    $('[data-clipboard-target]',this).each(function(){
        let clipboard = new ClipboardJS(this);
        clipboard.on('success', function (e) {
            if (typeof toastr !== 'undefined') {
                toastr.success('copié dans le presse-papier', 'SUCCESS');
            }
        });
    });

    $('[data-clipboard-text]',this).each(function(){
        let clipboard = new ClipboardJS(this);
        clipboard.on('success', function (e) {
            if (typeof toastr !== 'undefined') {
                toastr.success('copié dans le presse-papier', 'SUCCESS');
            }
        });
    });

}