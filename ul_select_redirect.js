$.fn.extend({
    'ul_select_redirect': ul_select_redirect
});

function ul_select_redirect(){
    $('ul.select_redirect',this).each(function(){
        $(this).after('<select><option></option></select>');
        let select_redirect=$(this).next();
        $('ul.select_redirect li a').each(function(){
            let classname=''
            if ($(this).hasClass('active')) {
                classname=' selected="selected"';
            }
            $(select_redirect).append('<option value="'+$(this).attr("href")+'"'+classname+' >'+ $(this).text()+'</option>');
        });
        $('ul.select_redirect').remove();
        $(select_redirect).change( function() {
            redirection(this.value);
        });
    });
}
