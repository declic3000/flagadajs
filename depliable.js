$.fn.extend({
	'initialisation_depliable': initialisation_depliable,
	'initialisation_depliable_bloc': initialisation_depliable_bloc,
	'initialisation_depliable_survol': initialisation_depliable_survol,
	'initialisation_voir_plus': initialisation_voir_plus
});

function initialisation_depliable() {
	$(this).initialisation_depliable_bloc();
	$(this).initialisation_depliable_survol();
	$(this).initialisation_voir_plus();
}

function initialisation_depliable_bloc() {
	let conteneur = this;

	$('.depliable:not(.initiated)',conteneur).each(function(){
		$(".gachette",$(this)).first()
			.prepend('<i class="fa fa-arrow-circle-right"></i> ')
			.click(function(){
				bloc_depliable = $(this).parents('.depliable')[0];
				if ($(bloc_depliable).hasClass('deplie')){
					$(this).children("svg.fa-arrow-circle-down").remove();
					$(this).prepend('<i class="fa fa-arrow-circle-right"></i> ')
					$(bloc_depliable).removeClass('deplie').children('.depliant').slideUp();
				}
				else
				{
					$(this).children("svg.fa-arrow-circle-right").remove();
					$(this).prepend('<i class="fa fa-arrow-circle-down"></i> ')
					$(bloc_depliable).addClass('deplie').children('.depliant').slideDown();
				}
			});
		if (!$(this).hasClass('deplie')){
			$(this).removeClass('deplie').children('.depliant').hide();
		}
		$(this).addClass('initiated');
	})
}

function initialisation_depliable_survol(){
	let conteneur = this;
	$('.depliable_survol:not(.initiated)',conteneur).each(function(){
		$(".gachette",$(this))
			.hover(function(){
				bloc_depliable = $(this).parents('.depliable_survol')[0];
				if ($(bloc_depliable).hasClass('deplie')){
					$(bloc_depliable).removeClass('deplie').children('.depliant').slideUp();
				}
				else
				{
					$(bloc_depliable).addClass('deplie').children('.depliant').slideDown();
				}
			});
		$(this).addClass('initiated');
	})
}

function initialisation_voir_plus(){

	let conteneur = this;

	$('.voir_plus',conteneur).each(function(){
		let hauteur=$(this).data('height');
		if (!(hauteur>1)){
			hauteur =300;
		}
		if ($(this).height() > hauteur){
			$(this).height(hauteur);
			$(this).append('<div class="voile_degrade"></div>');
			$(this).after('<div class="btn btn-block btn-primary gachette"><i class="fa fa-plus-circle"></i> En Voir plus</div>');
			$(this).next(".gachette").click(function(){
				$(this).prev('.voir_plus').height('auto').removeClass('voir_plus');
				$(this).remove();
			});
		}
		else {
			$(this).removeClass('voir_plus');
			$(this).height('auto');
		}
	});
}