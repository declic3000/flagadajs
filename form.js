$.fn.extend({
    'initialisation_composant_form': initialisation_composant_form,
    'initialisation_composant_forms': initialisation_composant_forms,
    'refuserToucheEntree': refuserToucheEntree
});

function initialisation_composant_forms() {
    $('form', this).each(function () {
        $(this).initialisation_composant_form();
    });
}


function initialisation_composant_form() {
    if (!$(this).hasClass('form_inited')) {

        let conteneur_form_div = $(this).parent('div.formulaire');

        $('input,textarea,select',this).bind('invalid', function(e){
            form = $(this).parents('form');
            form.addClass('was-validated');
            $(this).next('.invalid-feedback').remove();
            $(this).after('<div class="invalid-feedback">'+ $.prop(this, 'validationMessage') +'</div>');
        });

        $(this).bind('validated.bs.validator', function(e) {
            if ($('input:invalid,textarea:invalid,select:invalid',this).length>0) {
                $(this).addClass('has_error');
            }
            else
            {
                $(this).removeClass('has_error');
            }
        });



        /////////////////////////////////////
        // champs Date

        if (typeof $.fn.datetimepicker === "function") {


        $('.datepickerf', this).wrap('<div class="input-group"></div>');
        $('.datepickerf', this).before(' <div class="input-group-addon"><i class="fa fa-calendar"></i></div>');
        $('.datepickerf', this).attr('data-inputmask', "'alias': 'dd/mm/yyyy'");
        $(".datepickerf", this).inputmask("dd/mm/yyyy", {"placeholder": "jj/mm/aaaa"});
        $('.datepickerf', this).datetimepicker({
            'format': 'DD/MM/YYYY',
            'allowInputToggle': true,
            'maxDate': moment().add(1, 'days')
        });


        $('.datepickerb:not([type=date])', this)
            .wrap('<div class="input-group date"></div>')
            .after('<div class="input-group-append"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>')
            .attr('data-inputmask', "'alias': 'dd/mm/yyyy'")
            .inputmask("dd/mm/yyyy", {"placeholder": "jj/mm/aaaa"})
            .datetimepicker({'format': 'DD/MM/YYYY', 'allowInputToggle': true});

        $('.datetimepickerb', this)
            .wrap('<div class="input-group date"></div>')
            .after('<div class="input-group-append"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>')
            .attr('data-inputmask', "'alias': 'dd/mm/yyyy hh:ii'")
            .inputmask("datetime", {
                mask: "1-2-y h:s",
                placeholder: "dd-mm-yyyy hh:mm",
                leapday: "-02-29",
                separator: "-",
                alias: "dd-mm-yyyy"
            })
            .datetimepicker({'format': 'DD/MM/YYYY HH:mm', 'allowInputToggle': true});
        }

        /////////////////////////////////////
        // champs Heure
        if (typeof $.fn.timepicker === "function") {
            let options = {
                'timeFormat': 'H\\:i'
            }
            $('.timepicker').timepicker(options);
        }

        /////////////////////////////////////
        // champs fichier

        $(".jq-ufs", this).not("[multiple]").each(
            function (i) {
                $(this).after(
                    '<div id="fileuploader' + i + '" class="fileuploader" data-name-field="'
                    + $(this).attr('id') + '">Upload </div>');
            });

        $(".jq-ufs[multiple]", this).each(
            function (i) {
                $(this).after(
                    '<div id="fileuploadermultiple' + i + '" class="fileuploader-multiple" data-name-field="'
                    + $(this).attr('id') + '">Upload </div>');
            });

        $(".jq-ufs", this).hide();

        let options_fileuploader = {
            multiple: false,
            dragDrop: true,
            maxFileCount: 1,
            fileName: 'fichiers',
            dragDropStr: 'Faites glisser et déposez votre fichier ici',
            abortStr: "Abandonner",
            cancelStr: "Annuler",
            doneStr: "fait",
            multiDragErrorStr: "Plusieurs Drag &amp; Drop de fichiers ne sont pas autorisés.",
            extErrorStr: "n'est pas autorisé. Extensions autorisées:",
            sizeErrorStr: "Fichier trop volumineux",
            uploadErrorStr: "Upload n'est pas autorisé",
            uploadStr: "Choisir un fichier",
            statusBarWidth: "100%",
            dragdropWidth: '100%',
            showPreview: true,
            previewHeight: "auto",
            previewWidth: "100%"
        };

        $('.fileuploader', this).each(function () {
            options_fileuploader['url'] = $(this).parents('form').attr('data-url');
            $(this).uploadFile(options_fileuploader);
        });

        $(".fileuploader-multiple", this).each(function () {
            options_fileuploader['url'] = $(this).parents('form').attr('data-url');
            options_fileuploader['multiple'] = true;
            options_fileuploader['dragDropStr'] = 'Glisser et déposez vos fichiers ici';
            options_fileuploader['uploadStr'] = 'Selectionner un ou plusieurs fichiers';
            $(this).uploadFile(options_fileuploader);
        });


        /////////////////////////////////////
        // Select2

        let opts_partage={
            theme: "bootstrap",
            placeholder: '',
            allowClear: true,
            width: '200px',
            minimumResultsForSearch: 30,
            dropdownAutoWidth : true
        }

        $('select.select2,select.select2_simple,select.select2_hierarchie',this).each(function(){
            modal = $(this).parents('.modal');
            let opts = opts_partage;
            if (modal.length>0){
                let identifiant_modal = $(modal[0]).attr('id');
                opts['dropdownParent']=$("#"+identifiant_modal);
            }
            if ($(this).hasClass('select2_hierarchie')){
                opts['templateResult']= function(state) {
                    let classes  = $(state.element).attr('class')
                    return $('<span class=" ' + classes + '">' + state.text + '</span>');
                }
            }
            $(this).select2(opts);
        })



        /////////////////////////////////////
        // champs masqué
        $('textarea.champs_masque', this).each(function () {
            $(this).after('<div><input type="checkbox" class="bs_switch" class="switch_masquer" data-target="'+$(this).attr('id')+'" /> <label>Afficher le champs texte</label></div>');
            $(this).next().children('.bs_switch').click(function(){
                if ($(this).prop('checked')){
                    $('#'+$(this).attr('data-target')).slideDown();
                }
                else{
                    $('#'+$(this).attr('data-target')).hide();
                }
            })
        });


        /////////////////////////////////////
        // switch
        $('input.bs_switch', this).each(function () {
            $(this).parent().addClass("pretty p-switch p-fill");
            $(this).next('label').wrap('<div class="state"></div>');
        });


        /////////////////////////////////////
        // Bouton de soumission

        $(document).on("keypress", ".input-group:has(input:input, span.input-group-btn:has(div.btn)) input:input", function(e){
            if (e.which === 13){
                $(this).closest(".input-group").find("div.btn").trigger('click');
            }
        });

        // Pour permettre de savoir quel bouton a soumis le formulaire
        $("button[type=submit]", this).on("click", function (e) {
            $(this).addClass('cliclic');
        });

        // Desactive la soumission multiple des formulaires
        $(this).on("submit", function (e) {
            if (!$(this).hasClass('has_error')){
                $(":submit", this).on("click", function (e) {
                    e.preventDefault();
                });
            }
        })

        $('.desactiveSoumission',this).keypress(refuserToucheEntree);


        /////////////////////////////////////
        // Champs de mot de passe


        $('.password-field', this).focus(function () {
            $(this).removeAttr('readonly');
        });


        /////////////////////////////////////
        // Bulle d'aide

        $('.tout_cocher', this).each(function(){
            $(this).prepend('<div class="tout_cocher_action float-right"><span class="cocher">Tout cocher</span> - <span class="decocher">Tout cocher</span></div>');
            $('.tout_cocher_action .cocher',this).click(function(){ let cel = $(this).parent().parent(); $('input[type=checkbox]',cel).attr('checked','checked'); });
            $('.tout_cocher_action .decocher',this).click(function(){  let cel = $(this).parent().parent(); $('input[type=checkbox]',cel).removeAttr('checked'); });
        });




        /////////////////////////////////////
        // Bulle d'aide

        $('.help-block[data-toggle="tooltip"]', this).tooltip();


        /////////////////////////////////////
        // Controle de saisie

        $(this).validator().on('submit', function (e) {
            $(this).validator('validate');
            if ($(this).hasClass('has_error')) {
                return false;
            } else {
                // Ajoute un spinner lors de l'envoi d'un formulaire sauf si erreur dans formulaire
                let btn = $('button[type=submit]',this);
                if (!$(btn).hasClass('en_attente')) {
                    $(btn).addClass('en_attente').prepend(' <i class="fa fa-spinner fa-pulse fa-fw"></i> ');
                }
                return true;
            }
        })

        /////////////////////////////////////
        // inclusion d'autre initialisation

        $(conteneur_form_div).initialisation_modal_form();
        $(this).initialisation_bouton_lievre_tortue();
        $(this).initialisation_form_trier_par();
        if (typeof initialisation_choix_tri_colonne === "function") {
            $(this).initialisation_choix_tri_colonne();
        }
        if (typeof activer_majuscule_magique === "function") {
            $(this).activer_majuscule_magique();
        }
        if (typeof initialisation_form_autres_composants === "function") {

            $(this).initialisation_form_autres_composants();
        }



        /////////////////////////////////////
        // Marquer le formulaire comme initié


        $(this).addClass('form_inited')
    }
}





function refuserToucheEntree(event)
{
    // Compatibilité IE / Firefox
    if(!event && window.event) {
        event = window.event;
    }
    // IE
    if(event.keyCode === 13) {
        event.returnValue = false;
        event.cancelBubble = true;
    }
    // DOM
    if(event.which === 13) {
        event.preventDefault();
        event.stopPropagation();
    }
}